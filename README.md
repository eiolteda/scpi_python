# SCPI_python

SCPI sockets functions for python3
Functions to easily talk to SCPI instruments using sockets and Python. Based on MatC's from Keysights python2 functions https://community.keysight.com/thread/24432